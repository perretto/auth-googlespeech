﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using auth_googlespeech.Models;
using Microsoft.AspNetCore.Mvc;
using System.Configuration;
using MongoDB.Driver;
using Microsoft.Extensions.Configuration;

namespace auth_googlespeech.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SocketController : ControllerBase
    {
        private IConfiguration configuration;

        public SocketController(IConfiguration _configuration)
        {
            this.configuration = _configuration;
        }

        [HttpGet]
        public GoogleSpeechCredentials BuscarGoogleSpeechCredentials(string dominio)
        {
            try
            {
                MongoClient client = new MongoClient(this.configuration.GetValue<string>("ConexaoMongoDB:endereco"));
                IMongoDatabase db = client.GetDatabase("korus-central");

                var googleSpeechCredentials = db.GetCollection<GoogleSpeechCredentials>("google_speech_credentials");
                var item = googleSpeechCredentials.Find(i => i.dominio == dominio).FirstOrDefault();

                if (item != null)
                    return item;
                else
                    return null;
            }
            catch (Exception e)
            {
                GoogleSpeechCredentials googleSpeechCredentials = new GoogleSpeechCredentials();
                googleSpeechCredentials.auth_uri = e.Message;
                return googleSpeechCredentials;
            }
        }

        [HttpPost]
        public void InserirGoogleSpeechCredentials(string dominio, [FromBody]GoogleSpeechCredentials credentials)
        {
            MongoClient client = new MongoClient(this.configuration.GetValue<string>("ConexaoMongoDB:endereco"));
            IMongoDatabase db = client.GetDatabase("korus-central");

            var googleSpeechCredentials = db.GetCollection<GoogleSpeechCredentials>("google_speech_credentials");
            var item = googleSpeechCredentials.Find(i => i.dominio == dominio).FirstOrDefault();

            if (item == null)
            {
                credentials.dominio = dominio;
                googleSpeechCredentials.InsertOne(credentials);
            }
            else
            {
                item.type = credentials.type;
                item.project_id = credentials.project_id;
                item.private_key_id = credentials.private_key_id;
                item.private_key = credentials.private_key;
                item.client_email = credentials.client_email;
                item.client_id = credentials.client_id;
                item.auth_uri = credentials.auth_uri;
                item.token_uri = credentials.token_uri;
                item.auth_provider_x509_cert_url = credentials.auth_provider_x509_cert_url;
                item.client_x509_cert_url = credentials.client_x509_cert_url;

                googleSpeechCredentials.ReplaceOne(i => i.dominio == dominio, item);
            }
        }
    }
}
